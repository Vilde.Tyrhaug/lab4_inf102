package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickMedian implements IMedian {

  @Override
  public <T extends Comparable<T>> T median(List<T> list) {
    List<T> listCopy = new ArrayList<>(list); // Method should not alter the original list
    int n = listCopy.size();

    // Calculate the index of the median
    int medianIndex = n / 2;

    // Find the median element using Quickselect
    T medianValue = quickSelect(listCopy, 0, n - 1, medianIndex);

    return medianValue;
  }

  private <T extends Comparable<T>> T quickSelect(
    List<T> list,
    int left,
    int right,
    int k
  ) {
    if (left == right) {
      return list.get(left);
    }

    // Choose a random pivot element and partition the list
    int pivotIndex = partition(list, left, right);

    if (k == pivotIndex) {
      return list.get(k);
    } else if (k < pivotIndex) {
      return quickSelect(list, left, pivotIndex - 1, k);
    } else {
      return quickSelect(list, pivotIndex + 1, right, k);
    }
  }

  private <T extends Comparable<T>> int partition(
    List<T> list,
    int left,
    int right
  ) {
    // Choose a random pivot index
    int randomIndex = left + new Random().nextInt(right - left + 1);
    T pivotValue = list.get(randomIndex);

    // Move the pivot element to the end
    swap(list, randomIndex, right);

    int i = left;
    for (int j = left; j < right; j++) {
      if (list.get(j).compareTo(pivotValue) <= 0) {
        swap(list, i, j);
        i++;
      }
    }

    // Move the pivot element to its final position
    swap(list, i, right);

    return i;
  }

  private <T extends Comparable<T>> void swap(List<T> list, int i, int j) {
    T temp = list.get(i);
    list.set(i, list.get(j));
    list.set(j, temp);
  }
}
