package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

  @Override
  public <T extends Comparable<T>> void sort(List<T> list) {
    for (int i = 0; i < list.size(); i++) {
      boolean swapped = false;
      for (int j = 0; j < list.size() - i - 1; j++) {
        if (list.get(j).compareTo(list.get(j + 1)) > 0) {
          T tempStore = list.get(j);
          list.set(j, list.get(j + 1));
          list.set(j + 1, tempStore);
          swapped = true;
        }
      }
      if (!swapped) {
        break;
      }
    }
  }
}
